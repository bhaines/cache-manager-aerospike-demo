'use strict'

var cacheManager = require('cache-manager')
var clone = require('clone')
var async = require('async')

var testType = 'performance' // abstraction | performance
var cacheType = 'aerospike' // aerospike | redis | memory | aerospikePerformance

var maxCalls = 100000
var maxAsyncRequests = 299

function abstraction() {
	var cache = false
	var options = {}

	if ('aerospike' == cacheType) {
		// Aerospike cache
		var aerospikeStore = require('cache-manager-aerospike')
		cache = cacheManager.caching({
			"store": aerospikeStore,
			"host": "172.28.128.3",
			"port": 3000,
		})
		options['ns'] = 'rx',
		options['set'] = 'grapeshot'
	} else if ('redis' == cacheType) {
		// Redis cache
		var redisStore = require('cache-manager-redis')
		cache = cacheManager.caching({
		    store: redisStore,
		    host: '127.0.0.1', // default value
		    port: 6379
		})
	} else if ('memory' == cacheType) {
		// Memory cache
		cache = cacheManager.caching({
			store: 'memory',
			max: 100,
			ttl: 10
		})
	}

	console.log("\n\nTesing with " + cacheType + " cache store.")
	var key = "my_key"
	var data = {"key": "value"}

	var setOptions = clone(options)
	setOptions['ttl'] = 60 * 20 // twenty minutes

	cache.set(key, data, setOptions, (err)=>{
		console.log("Set success.")
		if (err) {
			console.log("Failed setting: " + err)
			return
		}
		cache.get(key, options, (err, value)=>{
			if (err) {
				console.log("Failed getting: " + err)
				return
			}
			console.log("Get success result:\n" + JSON.stringify(value))
			cache.del(key, options, (err)=>{
				if (err) {
					console.log("Failed deleting: " + err)
					return
				}
				console.log("Delete success.")

				// Optionally close the aerospike connection
				if ('aerospike' == cacheType) {
					cache.store.connection.close()
				}
			})
		})
	})
}

function performance() {
	async.series([
		function(callback) {
			console.log(`cache manager performance test ${maxCalls} calls on ${maxAsyncRequests} async connections`)
			var aerospikeStore = require('cache-manager-aerospike')
			var cache = cacheManager.caching({
				"store": aerospikeStore,
				"host": "172.28.128.3",
				"port": 3000,
			})
			var options = {
				'ns': 'rx',
				'set': 'grapeshot'
			}

			var keyRoot = 'aaa'
			var startDate = new Date()
			var startTime = startDate.getTime()
			async.timesLimit(maxCalls, maxAsyncRequests, (i, next)=>{
				cache.set(keyRoot + i, 'value' + i, options, (err)=>{
					if (err) {
						console.log("set error: " + err)
						return
					}
					cache.get(keyRoot + i, options, (err, value)=> {
						if (err) {
							console.log("set error: " + err)
							return
						}
						// if (0 == i%100) {
						// 	console.log("Got: " + value)
						// }
						// var timeout = Math.floor(Math.random() * 100)
						// setTimeout(function() {
							next()
						// }, timeout)
					})
				})
			}, function() {
				var endDate = new Date()
				console.log("Done! MS: " + (endDate.getTime() - startTime))
				cache.store.connection.close()
				callback()
			})
		},
		function(callback) {
			console.log(`aerospike module performance test ${maxCalls} calls on ${maxAsyncRequests} async connections`)
			var Aerospike = require('aerospike')
			const Key = Aerospike.Key
			var policy = { "exists": Aerospike.policy.exists.CREATE_OR_REPLACE }
			Aerospike.connect({"hosts": "172.28.128.3:3000"}, (err, client)=>{
				if (err) {
					Console.log("Connect failure")
					process.exit(1)
				}
				var keyRoot = 'aaa'
				var startDate = new Date()
				var startTime = startDate.getTime()
				async.timesLimit(maxCalls, maxAsyncRequests, (i, next)=>{
					var keyObject = new Key('rx', 'grapeshot', keyRoot + i)
					client.put(keyObject, {"__string": JSON.stringify(keyRoot + i)}, {}, policy, (err)=>{
						client.get(keyObject, (err, record, meta)=>{
							if (err) {
								console.log("set error: " + err)
								process.exit(1)
								return
							}
							next()
						})
					})
				}, function() {
					var endDate = new Date()
					console.log("Done! MS: " + (endDate.getTime() - startTime))
					client.close()
					callback()
				})
			})
		},
		function(callback) {
			callback()
		}
	])
}

function main(test) {
	if ('abstraction' == test) {
		abstraction()
	} else {
		performance()
	}
}

main(testType)